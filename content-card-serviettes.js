class ContentCardServiettes extends HTMLElement {

    _elements = {};
    _entities = {};

    _options = [
        { "key": "frost_protection",
          "icon": "mdi:snowflake",
          "color": "#0022ff" },
        { "key": "eco",
          "icon": "mdi:weather-partly-cloudy",
          "color": "#f08100" },
        { "key": "comfort",
          "icon": "mdi:weather-sunny",
          "color": "#ec001c" }
    ];

    constructor() {
        super();
        this.createCard();
        this.createStyle();
        this.attach();
        this.getElements();
        this.listen();
    }

    createCard() {
        this._card = document.createElement("ha-card");
        var h = `
          <div id="states" class="card-content">
            <div class="serviettes row">
              <ha-icon icon="" class="device-icon"></ha-icon>
              <span class="serviettes name"></span>
              <span class="options-icons">
          `;
        for(var o of this._options) {
            h += `<ha-icon icon="${o.icon}" class="opt-${o.key} opt-off"></ha-icon>`;
        }
        h += `
              </span>
              <span class="serviettes value"></span>
            </div>
          </div>
      `;
        this._card.innerHTML = h;
    }

    createStyle() {
        this._style = document.createElement("style");
        var css = `
      .device-icon.show {
         color: var(--paper-item-icon-color);
         flex-basis: 40px;
         display: flex;
         justify-content: center;
      }
      .serviettes.name {
         margin-left: 16px;
      }
      .serviettes.row {
         display: flex;
         flex-direction: row;
      }
      .serviettes.value {
         width: 5em;
         text-align: right;
      }
      .options-icons {
        margin-left: auto;
      }
      .opt-off {
        color: #c7c7c7;
      }
      .opt-off:hover {
        color: #838383;
        cursor: pointer;
      }
      `;
        for(var o of this._options) {
            css += `
      .opt-${o.key}.opt-on {
        color: ${o.color};
      }
      `;
        }
        this._style.textContent = css;
    }

    attach() {
        this.attachShadow({ mode: "open" });
        this.shadowRoot.append(this._style, this._card);
    }

    getElements() {
        this._elements.name = this._card.querySelector(".name");
        this._elements.value = this._card.querySelector(".value");
        this._elements.icon = this._card.querySelector(".device-icon");
        for(var o of this._options) {
            this._elements[o.key] =
                this._card.querySelector(".opt-" + o.key);
        }
    }

    setConfig(config) {
        if (!config.device) {
            throw new Error("You need to define an device");
        }
        this.config = config;
        if(this.config.name) {
            this._elements.name.innerHTML = this.config.name;
        }
        if(this.config.icon) {
            this._elements.icon.icon = this.config.icon;
            this._elements.icon.classList.add("show");
        }
    }

    getState(k) {
        return this._hass.states[this._entities[k]];
    }

    getEntities() {
        const deviceId = this.config.device;
        const device = this._hass.devices[deviceId];

        if(!this.config.name) {
            this._elements.name.innerHTML = device.name;
        }

        // Populate entities with
        // power, energy, option
        for(var e in this._hass.entities) {
            const ee = this._hass.entities[e];
            if(ee.device_id == deviceId) {
                var s = this._hass.states[e];
                var c = s.attributes["device_class"];
                var o = s.attributes["options"];
                if(c) {
                    this._entities[c] = e;
                } else if( o && o.includes('comfort') ) {
                    this._entities.option = e;
                }
            }
        }
    }

    set hass(hass) {
        this._hass = hass;
        if(!this._entities.options) {
            this.getEntities();
        }

        const active = this.getState("option").state;
        for(var o of this._options) {
            if(o.key == active) {
                this._elements[o.key].classList.add("opt-on");
                this._elements[o.key].classList.remove("opt-off");
            } else {
                this._elements[o.key].classList.add("opt-off");
                this._elements[o.key].classList.remove("opt-on");
            }
        }

        const pow = this.getState("power");
        this._elements.value.innerHTML = `${pow.state} ${pow.attributes.unit_of_measurement}`;

        // debug
        window.serviette=this;
        window.hass=hass;
    }

    listen() {
        for(var o of this._options) {
            this._elements[o.key].addEventListener("click", this.setOption.bind(this, o), false);
        }
    }

    setOption(o) {
        this._hass.callService('select', 'select_option', {
            entity_id: this._entities["option"],
            option: o.key
        });
    }

    getCardSize() {
        return 1;
    }
}

customElements.define("content-card-serviettes", ContentCardServiettes);
